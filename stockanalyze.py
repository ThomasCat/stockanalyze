blurb = '''Stockanalyze v1.0'''
copyrightnotice = '''Copyright © 2021 Owais Shaikh'''

def clear():
    _ = subprocess.call('clear' if os.name =='posix' else 'cls')

try:
    import subprocess, time, os, datetime, math, sys
    from datetime import date
    from time import strptime
    import json
    from gnews import GNews
    import yfinance as yf
    from goose3 import Goose
    import plotext
    import termplot
    import pandas
    import requests
    from polyglot.text import Text
    import tk
    from polyglot.downloader import downloader
    print("\nGetting latest language data...")
    downloader.download("sentiment2.en", quiet=True)
    downloader.download("embeddings2.en", quiet=True)
    downloader.download("ner2.en", quiet=True)

except ImportError:
    #import traceback
    #traceback.print_exc()
    print("\nInstalling missing libraries. This can take some time...")
    time.sleep(3)
    subprocess.call(['pip', 'cache', 'purge'])
    url = 'git+https://github.com/aboSamoor/polyglot.git@master'
    subprocess.call(['pip', 'install', '-r', 'requirements.txt'])
    subprocess.call(['pip', 'uninstall', 'pyicu', '-y'])
    subprocess.call(['pip', 'cache', 'purge'])
    subprocess.call(['pip', 'install', 'pyicu'])
    time.sleep(1)
    clear()
    print("\nDone! Restart program... :)")
    sys.exit()

def getCompanyName (company_code):
    name = yf.Ticker(company_code)
    company_name = name.info['shortName']
    return company_name

def getCompanyCode (company_name):
    url = "https://s.yimg.com/aq/autoc"
    parameters = {'query': company_name, 'lang': 'en-US'}
    response = requests.get(url = url, params = parameters)
    data = response.json()
    company_code = data['ResultSet']['Result'][0]['symbol']
    return company_code

def getNewsArticles (search_string, time_period, results):
    news_urls = {}
    google_news = GNews(language='en', period=time_period, max_results=results)
    response = google_news.get_news(search_string)
    for article in response:
        url = article.get('url')
        date = article.get('published date')
        day = date.split(' ')[1]
        month = strptime(date.split(' ')[2],'%b').tm_mon
        year = date.split(' ')[3]
        datestamp = str(year) + str(month) + str(day) 
        news_urls[datestamp] = url

    sorted_news_urls = sorted(news_urls.items())
    return dict(sorted_news_urls).values()

def extractNewsData (url):
    try:
        response = requests.get(url)
        extractor = Goose()
        article = extractor.extract(raw_html=response.content)
        text = article.cleaned_text
        return text
    except:
        pass

def plottableSentiment (paragraph):
    try:
        processed_text = Text(paragraph)
        score = processed_text.polarity
        score = score + 1.0 
        score = (score/2) * 100
        return int(score)
    except:
        return 0 #incompatible, unreachable or corrupt articles

def plotGraph (reputation, price):
    xaxis = []
    for i in range (1, len(reputation)): xaxis.append(i) # used to set x axis length to number of news articles
    plotext.plot (xaxis, reputation)
    plotext.plot (xaxis, price)
    try:
        plotext.show ()
    except OSError: #unsupported terminal
        for b in range (0, len(reputation)): # spread graph out
            reputation.insert (b * 2, 0)

        for b in range (0, len(price)): # spread graph out
            price.insert (b * 2, 0)

        print('\n--------------------------\n')
        print ("Reputation graph: ")
        termplot.plot (reputation)
        print('\n--------------------------\n')
        print ("Price graph: ")
        termplot.plot (price)

def reputationInEnglish (avg):
    if avg >= 0 and avg <= 25: return "terrible"
    if avg >= 25 and avg <= 50: return "mediocre"
    if avg >= 50 and avg <= 75: return "good"
    if avg >= 75 and avg <= 100: return "fantastic"

def getStockData (company_info, time_period):
    company = yf.Ticker(company_info)
    data = company.history(period = time_period, interval='1d')
    close_data = dict(data.Close).items()
    close_prices = list(dict(data.Close).values())
    dates = []

    for data in close_data:
        key = str(data[0]).replace("-", "").partition(" ")
        key = key[0]
        dates.append(key)

    prices = dict(zip(dates, close_prices))
    sorted_prices = sorted(prices.items())
    return dict(sorted_prices).values()

def makePrediction ():
    return

def convertPriceToPlottable (price_list, company_info):
    company = yf.Ticker(company_info)
    data = company.history(period = str(len(price_list))+'d', interval='1d')
    data = list(data.Close)

    highest = max(data)
    lowest =  min(data)
    percentaged_list = []
    for price in price_list:
        percentage = ((price - lowest) * 100) / (highest - lowest)
        percentaged_list.append (percentage)
    
    return percentaged_list


if __name__ == "__main__":
    clear()
    time_period = 7 # period in days (default: 1 week / 7 days)
    results = 100
    today = date.today()
    print (blurb + " (" + today.strftime("%B %d, %Y")  + ")\n" + copyrightnotice + "\n")
    if len(sys.argv) == 1:
        company = input ("Enter company name / symbol (Eg. GameStop or GME) ▶ ")
    
    if len(sys.argv) == 2:
        company = sys.argv[1]
        print ("Enter company name / symbol ▶ " + company)
        
    if len(sys.argv) == 3:
        company = sys.argv[1]
        print ("Enter company name / symbol ▶ " + company)
        time_period = int(sys.argv[2])
        if time_period != 0: print ("Time period ▶ " + str(time_period) + " days")

    try:
        company = getCompanyCode(company)

        print('\nGetting '+ getCompanyName(company) + ' (' + company + ')' + ' stock prices for the past '+ str(time_period) +' days')
        prices = getStockData (company, str(time_period)+'d')

        print ('Getting news articles about '+ getCompanyName(company))
        search_string = getCompanyCode(company)
        article_urls = getNewsArticles(search_string ,str(time_period)+'d', results)

        print ('Getting news articles for the past '+ str(time_period) +' days')

        sentiment_data = []
 
        print ("Analyzing market sentiment (this may take a while if the time period is large)\n")
        index = 1
        for url in article_urls:
            text = extractNewsData(url)
            sentiment_data.append (plottableSentiment (text))
            print ("Scanning article " + str(index))
            index+=1

        plotext.title(getCompanyName(company) + "'s reputation (blue) and stock price (orange) over " + str(time_period) + " day period.")
        plotGraph (sentiment_data, convertPriceToPlottable (prices, company))

        avg = sum(sentiment_data)/len(sentiment_data)

        print ("\nFor the past " + str(time_period) + " days, " + getCompanyName(company) + " had a " + reputationInEnglish(avg) + " reputation (" + str(int(avg)) +"%).")
    
    except IndexError:
        clear()
        print ("Couldn't gather enough data on " + company + ".")

    except requests.exceptions.ConnectionError: 
        clear()
        print ("Couldn't connect to the internet. Please check your firewall, adblocker and network and try again.")
