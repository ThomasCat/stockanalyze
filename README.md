[![Python](https://img.shields.io/badge/Made%20in-Python%20-3e6d95.svg?style=flat-square&logo=python)](https://python.org)
[![License](https://img.shields.io/badge/License-Apache%202.0-purple?style=flat-square)](LICENSE)
[![Open Source](https://img.shields.io/badge/Open%20Source-Yes-blue?style=flat-square)](https://opensource.org/)
[![Maintenance](https://img.shields.io/badge/Maintained-No-red.svg?style=flat-square)](https://gitlab.com/ThomasCat/stockanalyze/-/network/main)
[![Build](https://img.shields.io/gitlab/pipeline/thomascat/shouldibuy/main?style=flat-square)](https://gitlab.com/thomascat/stockanalyze/-/pipelines)

# Stockanalyze

Stockanalyze is a simple project I wrote for my Machine Learning class ([18CSL67](https://vtu.ac.in/pdf/2014syll/cs.pdf)). It determines if a company is safe to invest in based on public reputation via recent news articles, and correlates that with historical data to find if the stock will keep plunging or it will rise. 

## OBJECTIVE

The objective of Stockanalyze is to study about a company’s reputation and check to see if it
impacts their stock prices. This is done using the Polyglot NLP library to scrape through news articles
on Google and check if they’re talking about the company positively or negatively. If a majority of
the news is positive, then the market morale would be high, motivating investments to be made. If
the news is negative, this may cause a panic selling of stock prices. Histograms are used to
correlate this data to test if our theory applies or not. The data derived will be stock closing prices
that have been optimized via Machine Learning, via the Yahoo! Finance API.

<img src='demo.png'>

## SETUP

1. First, make sure your machine has the following:
    - An [internet connection](http://fixwifi.it/)
    - [Python 3](https://www.python.org/downloads/)
    - The [Python package manager](https://pip.pypa.io/en/stable/installing/)
    - [Tkinter](https://tkdocs.com/tutorial/install.html)

2. Download this project archive and extract it _or_ clone this project using `git clone https://gitlab.com/ThomasCat/stockanalyze.git`

3. Run Stockanalyze using `python3 stockanalyze.py`. During the first run, it will install some packages. Please wait for a while as it does so.

## USAGE

Run stockanalyze by typing 
```
python stockanalyze.py
```
in terminal. Enter a company name when prompted. To change the data, such as time period, simply edit the `time_period` variable in the script.

You can also pass the company name and time period (in days) as command-line arguments, for example, 
```
python stockanalyze.py samsung 10
```

This returns two graphs, one for public sentiment about the company, and another for stock prices. Both exist during the same time period and can be compared for observation.

### Example: Apple, Inc.

When this demo was run, Apple, Inc. just had their 2021 Worldwide Developers Conference
(WWDC21). Several new products, including iOS 15 and macOS Monterrey were released. As a
result of the new features being covered, the news reports for this particular week were positive.
Thus, the stock price of Apple also rose for the week due to general public sentiment.

The output should look similar to this...

<img src='demo2.png'>

## LIBRARIES USED

- goose3 – Goose is an article extractor written in Python and Scala, used to get paragraphs of text
from HTML and Wordpress pages.

- yfinance – The Yahoo! Finance API, used to get a dataset of stock closing prices to perform
numerical analysis on.

- gnews – Google News, used to get top news articles in a time period from reputed sources.

- polyglot – A natural language processing library, used to analyze the sentiment of news paragraphs.

- icu – International Components for Unicode, i18n libraries used by the Unicode Standard, to parse
text characters and make sure invalid characters are removed and exceptions aren’t raised.

- numpy – NumPy is a Python library used for working with arrays. It also has functions for working
in domain of linear algebra, fourier transform, and matrices.

- termplot – Terminal Plot, used to plot graphs and regression terms in the terminal against their
predictors.


## LICENSE

Yahoo! Finance API property of Yahoo, Inc.

Multimedia licensed under [![License: CC BY-NC-SA 4.0](https://licensebuttons.net/l/by-nc-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-nc-sa/4.0/) 

[Copyright © 2021 Owais Shaikh](LICENSE)
